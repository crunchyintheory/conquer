using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class PostProcessing : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Volume>().profile.TryGet(out DepthOfField dof);
        if (QualitySettings.GetQualityLevel() <= 2)
        {
            if (dof)
            {
                dof.active = false;
            }
        }
        else
        {
            if (dof)
            {
                dof.active = true;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
