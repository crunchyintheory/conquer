using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[DisallowMultipleComponent]
public class DirectorScoreboard : MonoBehaviour
{
    public Text redResources;
    public Text redUnits;
    public Image redImg1;
    public Image redImg2;
    public Image redBG;
    public Text timer;
    public Text blueResources;
    public Text blueUnits;
    public Image blueImg1;
    public Image blueImg2;
    public Image blueBG;

    private Color colorDisabled = new Color(1, 1, 1, 0.4f);
    private Color colorWhite = new Color(1, 1, 1, 1);
    private Color colorGray = new Color(0.27f, 0.27f, 0.27f);

    internal float time = 0;

    [Header("Game Over UI")]
    public Canvas GameOverUI;
    public Image GameOverUIBackground;
    public Text GameOverUIHeaderText;
    public Text GameOverUIText;
    public Sprite GameOverUIBGBlue;
    public Sprite GameOverUIBGRed;
    public Sprite GameOverUIBGGray;

    public void StartGame()
    {
        time = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        float seconds = Time.time - time;
        float minutes = Mathf.Floor((seconds) / 60);
        timer.text = string.Format("{0:00}:{1:00}", minutes, seconds % 60);
    }

    public void TurnComplete(byte stage)
    {
        if ((stage & 4) == 4)
        {
            blueBG.color = colorWhite;
            redBG.color = colorDisabled;
            blueResources.color = colorGray;
            blueUnits.color = colorGray;
            redResources.color = colorWhite;
            redUnits.color = colorWhite;
            blueImg1.color = colorGray;
            blueImg2.color = colorGray;
            redImg1.color = colorWhite;
            redImg2.color = colorWhite;
        } 
        else
        {
            blueBG.color = colorDisabled;
            redBG.color = colorWhite;
            blueResources.color = colorWhite;
            blueUnits.color = colorWhite;
            redResources.color = colorGray;
            redUnits.color = colorGray;
            blueImg1.color = colorWhite;
            blueImg2.color = colorWhite;
            redImg1.color = colorGray;
            redImg2.color = colorGray;
        }
    }

    public void UpdateBoard(Director.Team team)
    {
        if(team == Director.Team.Blue)
        {
            blueResources.text = Director.Instance.BlueResources.ToString();
            blueUnits.text = Director.Instance.BlueUnits.ToString();
            //blueLabel.text = $"{director.BlueResources,5}{director.BlueUnits,7}";
        }
        else
        {
            redResources.text = Director.Instance.RedResources.ToString();
            redUnits.text = Director.Instance.RedUnits.ToString();
            //redLabel.text = $"{director.RedResources,-7}{director.RedUnits,-5}";
        }
    }

    public void GameOver(Director.Team winner)
    {
        switch (winner)
        {
            case Director.Team.Blue:
                GameOverUIHeaderText.text = "Blue Wins!";
                GameOverUIBackground.sprite = GameOverUIBGBlue;
                break;
            case Director.Team.Red:
                GameOverUIHeaderText.text = "Red Wins!";
                GameOverUIBackground.sprite = GameOverUIBGRed;
                break;
            default:
                GameOverUIHeaderText.text = "Draw";
                GameOverUIBackground.sprite = GameOverUIBGGray;
                break;
        }

        Stats stats = Director.Instance.stats;
        float seconds = Time.time - time;
        float minutes = Mathf.Floor((seconds) / 60);

        GameOverUIText.text = @$"Turns: {stats.Turns:N0}
Time: {string.Format("{0:00}:{1:00}", minutes, seconds % 60)}

Blue Built:
  - Factories: {stats.BlueFactories:N0}
  - Barracks: {stats.BlueBarracks:N0}
  - Units: {stats.BlueUnits:N0}
  - Resources: {stats.BlueResources:N0}

Red Built:
  - Factories: {stats.RedFactories:N0}
  - Barracks: {stats.RedBarracks:N0}
  - Units: {stats.RedUnits:N0}
  - Resources: {stats.RedResources:N0}";

        GameOverUIBackground.GetComponent<Animator>().Play("GameOverFlyin");
    }
}
