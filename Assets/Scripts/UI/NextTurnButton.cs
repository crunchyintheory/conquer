using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class NextTurnButton : MonoBehaviour, IPointerClickHandler
{
    public GameBoardBehavior board;

    // Start is called before the first frame update
    void Start()
    {

    }

    public void OnPointerClick(PointerEventData eventData)
    {
        board.Select(board.selected);
        board.EndTurn();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
