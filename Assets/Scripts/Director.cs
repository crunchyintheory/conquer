﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public struct Stats
{
    public int Turns;
    public float TimeStart;
    public float TimeEnd;

    public int BlueFactories;
    public int BlueBarracks;
    public int BlueUnits;
    public int BlueResources;

    public int RedFactories;
    public int RedBarracks;
    public int RedUnits;
    public int RedResources;
}

[DisallowMultipleComponent, RequireComponent(typeof(AudioSource))]
public class Director : MonoBehaviour
{
    public static Director Instance;

    public enum Team
    {
        Blue,
        Red,
        None
    }

    /*
     * 1,1 - Game Completed
     * 10,2 - Game Ongoing
     * 100,4 - Blue Turn
     * 000,4 - Red Turn
     * 1000,8 - Local Game
     * 0000,8 - Online Game
     * 10000000,128 - Main Menu
     */
    public byte GameStage { get; internal set; } = 128;

    public Transform redWallet;
    public Transform blueWallet;
    public GameObject coin;
    public RectTransform hexHighlightUI;
    public GameBoardBehavior board;
    public Canvas gameUI;
    public Canvas mainMenu;
    public Animator table;

    [Header("Scoreboard")]
    public int RedResources;
    public int BlueResources;

    public int BlueUnits = 1;
    public int RedUnits = 1;

    [Header("Camera")]
    public float baseFov;
    public float baseRatio;
    public Camera cam { get; private set; }
    public float MinWideRes;
    public Vector2[] DefaultHighlightUIPosition;
    public Vector2[] WideHighlightUIPosition;

    [Header("Audio")]
    public AudioClip coinFallOneClip;
    public AudioClip coinFallManyClip;
    public AudioClip rotateClip;


    [Header("")]
    public Stats stats;

    public DirectorScoreboard scoreboard { get; private set; }
    private AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        if (Instance) Destroy(Instance);
        Instance = this;
        DontDestroyOnLoad(this);

        audioSource = GetComponent<AudioSource>();

        scoreboard = GetComponent<DirectorScoreboard>();

        Cursor.visible = true;
        cam = GetComponent<Camera>();

        switch(SystemInfo.deviceType)
        {
            case DeviceType.Desktop:
                if(SystemInfo.batteryStatus == BatteryStatus.Discharging) {
                    QualitySettings.SetQualityLevel(2);
                    break;
                }
                if(SystemInfo.graphicsMemorySize <= 2048)
                {
                    QualitySettings.SetQualityLevel(2);
                    break;
                }
                break;

            case DeviceType.Handheld:
                switch(Application.platform)
                {
                    case RuntimePlatform.IPhonePlayer:
                        if(SystemInfo.systemMemorySize <= 1024)
                        {
                            QualitySettings.SetQualityLevel(0);
                        }
                        else if(SystemInfo.systemMemorySize >= 3072)
                        {
                            QualitySettings.SetQualityLevel(3);
                        }
                        break;
                    default:
                        if(SystemInfo.systemMemorySize <= 2048)
                        {
                            QualitySettings.SetQualityLevel(0);
                        }
                        else if(SystemInfo.systemMemorySize >= 6144)
                        {
                            QualitySettings.SetQualityLevel(3);
                        }
                        break;
                }
                break;
        }
    }

    public void StartGame()
    {
        board.CreateBoard();
        GameStage |= 6;
        mainMenu.gameObject.SetActive(false);
        gameUI.gameObject.SetActive(true);
        stats = new Stats();
        scoreboard.StartGame();
    }

    // Update is called once per frame
    void Update()
    {
        /*float ratio = (float)cam.pixelWidth / cam.pixelHeight;
        cam.fieldOfView = baseFov * baseRatio / Mathf.Min(2.75f, ratio);
        if(ratio > MinWideRes)
        {
            hexHighlightUI.anchorMin = WideHighlightUIPosition[0];
            hexHighlightUI.anchorMax = WideHighlightUIPosition[1];
            hexHighlightUI.pivot = WideHighlightUIPosition[2];
        }
        else
        {
            hexHighlightUI.anchorMin = DefaultHighlightUIPosition[0];
            hexHighlightUI.anchorMax = DefaultHighlightUIPosition[1];
            hexHighlightUI.pivot = DefaultHighlightUIPosition[2];
        }*/
    }

    public void TurnComplete()
    {
        GameStage ^= 4;
        scoreboard.TurnComplete(GameStage);
        if((GameStage & 4) == 4)
        {
            table.Play("TableRotate");
        }
        else
        {
            table.Play("TableRotateBack");
        }
        audioSource.PlayOneShot(rotateClip);
        stats.Turns++;
    }

    public void AddCoin(Team player, int amount = 1)
    {
        Transform w;
        if (player == Team.Blue)
        {
            if (amount > 0) stats.BlueResources += amount;
            if (amount < 0 && BlueResources < Mathf.Abs(amount))
            {
                BlueResources = 0;
            }
            else
            {
                BlueResources += amount;
            }
            w = blueWallet;
        }
        else
        {
            if (amount > 0) stats.RedResources += amount;
            if (amount < 0 && RedResources < Mathf.Abs(amount))
            {
                RedResources = 0;
            }
            else
            {
                RedResources += amount;
            }
            w = redWallet;
        }
        if (amount < 0)
        {
            for(int i = 0; i < -amount; i++)
            {
                Destroy(w.GetChild(i).gameObject);
            }
        }
        else
        {
            Vector3 rpos;
            GameObject child;
            for(int i = 0; i < amount; i++)
            {
                rpos = new Vector3(Random.Range(-0.05f, 0.05f), 0, Random.Range(-0.04f, 0.04f));
                child = Instantiate(coin, w.position + rpos, coin.transform.rotation, w);
                child.SetActive(true);
                child.GetComponent<Rigidbody>().AddRelativeTorque(new Vector3(Random.value, Random.value, Random.value) * 3, ForceMode.VelocityChange);
            }

            if (amount > 3)
                audioSource.PlayOneShot(coinFallManyClip);
            else
                audioSource.PlayOneShot(coinFallOneClip);
        }
        scoreboard.UpdateBoard(player);
    }

    public void ExitButton()
    {
        if ((GameStage & 4) == 4)
            GameOver(Team.Red);
        else
            GameOver(Team.Blue);
    }

    public void GameOver(Team winner)
    {
        GameStage = 1;
        hexHighlightUI.gameObject.SetActive(false);
        gameUI.gameObject.SetActive(false);
        scoreboard.GameOverUI.gameObject.SetActive(true);
        scoreboard.GameOver(winner);
    }

    public void ExitToTitle()
    {
        GameStage = 128;
        AddCoin(Team.Blue, -BlueResources);
        AddCoin(Team.Red, -RedResources);
        board.DestroyBoard();
        BlueUnits = 1;
        RedUnits = 1;
        mainMenu.gameObject.SetActive(true);
        hexHighlightUI.gameObject.SetActive(false);
        scoreboard.GameOverUI.gameObject.SetActive(false);
        scoreboard.TurnComplete(4);
        table.Play("TableRotate", -1, 1);
    }
}
