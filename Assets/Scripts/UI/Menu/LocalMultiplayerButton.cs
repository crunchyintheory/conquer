using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class LocalMultiplayerButton : MonoBehaviour, IPointerClickHandler
{
    // Start is called before the first frame update
    void Start()
    {

    }

    public void OnPointerClick(PointerEventData eventData)
    {
        Director.Instance.GameStage = 8;
        Director.Instance.StartGame();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
