﻿using System.IO;
using UnityEngine;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;

class LicensingBuildProcessor : IPostprocessBuildWithReport
{
    public int callbackOrder { get { return 0; } }
    public void OnPostprocessBuild(BuildReport report)
    {
        string path = Path.GetDirectoryName(report.summary.outputPath);
        string root = Path.GetDirectoryName(Application.dataPath);

        File.Copy(Path.Combine(root, "LICENSE.txt"), Path.Combine(path, "LICENSE.txt"));
        File.Copy(Path.Combine(root, "THIRDPARTY.txt"), Path.Combine(path, "THIRDPARTY.txt"));
    }
}

