using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstructBehavior : MonoBehaviour
{
    public Material neutralMaterial;
    public Material blueMaterial;
    public Material redMaterial;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetTeam(Director.Team team)
    {
        if(team == Director.Team.Blue)
        {
            GetComponent<MeshRenderer>().material = blueMaterial;
        }
        else if (team == Director.Team.Red)
        {
            GetComponent<MeshRenderer>().material = redMaterial;
        }
        else
        {
            GetComponent<MeshRenderer>().material = neutralMaterial;
        }
    }
}
