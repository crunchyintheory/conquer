﻿public enum BuildingType
{
    None = 0,
    Barracks = 1,
    Factory = 2
}