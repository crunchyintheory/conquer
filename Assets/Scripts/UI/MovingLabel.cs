using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MovingLabel : MonoBehaviour
{
    internal short Current = 0;
    internal short Max = 0;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Text>().text = $"{Current} / {Max}";
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Change(bool increasing)
    {
        if(increasing)
        {
            if (Current >= Max) return;
            Current++;
        }
        else
        {
            if (Current == 1) return;
            Current--;
        }
        GetComponent<Text>().text = $"{Current} / {Max}";
    }

    public void Reset(short max)
    {
        Max = max;
        Current = (short)Mathf.Min(1, max);

        GetComponent<Text>().text = $"{Current} / {Max}";
    }
}
