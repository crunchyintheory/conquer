using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class OptionsMenuManager : MonoBehaviour
{
    private string[] displayModes = { "Fullscreen", "Borderless", "Maximized Window", "Windowed" };
    private int displayIndex;

    private Resolution[] resolutions;
    private int resolutionIndex;

    private string[] quality;
    private int qualityIndex;

    private string[] aaTypes = { "Off", "FXAA", "SMAA", "MSAA 2x", "MSAA 4x", "SSAA 2x", "SSAA 4x" };
    private int aaIndex;

    private string[] aoTypes = { "Off", "SSAO" };
    private int aoIndex;

    private string[] dofTypes = { "Off", "Gaussian", "Bokeh" };
    private int dofIndex;

    public Text displayT;
    public Text resolutionT;
    public Text qualityT;
    public Text aoT;
    public Text aaT;
    public Text dofT;

    [Header("Targets")]
    public Volume postProcessing;
    new public Camera camera;
    public ForwardRendererData forwardRendererData;
    private ScriptableRendererFeature ao;
    private UniversalAdditionalCameraData cameraData;
    private UniversalRenderPipelineAsset pipeline;
    private DepthOfField dof;

    [Header("Navigation")]
    public Animator parent;

    private void defaultSettings(bool save = false)
    {
        switch(QualitySettings.antiAliasing)
        {
            case 2:
                aaIndex = 3;
                break;
            case 4:
                aaIndex = 4;
                break;
        }

        switch(QualitySettings.GetQualityLevel())
        {
            case 0:
                aaIndex = 1;
                aoIndex = 0;
                dofIndex = 0;
                break;
            case 1:
                aaIndex = 2;
                aoIndex = 1;
                dofIndex = 1;
                break;
            case 2:
                aoIndex = 1;
                dofIndex = 1;
                break;
            case 3:
                aoIndex = 1;
                dofIndex = 2;
                break;
        }

        if(save) saveSettings();
    }

    private void loadSettings()
    {
        aaIndex = PlayerPrefs.GetInt("aaSetting", -1);
        aoIndex = PlayerPrefs.GetInt("aoSetting", -1);
        dofIndex = PlayerPrefs.GetInt("dofSetting", -1);

        if (aaIndex == -1) defaultSettings(true);
    }

    private void saveSettings()
    {
        PlayerPrefs.SetInt("aaSetting", aaIndex);
        PlayerPrefs.SetInt("aoSetting", aoIndex);
        PlayerPrefs.SetInt("dofSetting", dofIndex);
        PlayerPrefs.Save();
    }

    // Start is called before the first frame update
    void Start()
    {
        displayIndex = (int)Screen.fullScreenMode;
        displayT.text = $"Display: {displayModes[displayIndex]}";

        resolutions = Screen.resolutions;
        resolutionT.text = $"Resolution: {Screen.currentResolution.width}x{Screen.currentResolution.height} @ {Screen.currentResolution.refreshRate}Hz";
        resolutionIndex = System.Array.IndexOf(resolutions, Screen.currentResolution);

        quality = QualitySettings.names;
        qualityIndex = QualitySettings.GetQualityLevel();
        qualityT.text = $"Quality: {quality[qualityIndex]}";

        ao = forwardRendererData.rendererFeatures.Find(x => x.name == "NewScreenSpaceAmbientOcclusion");

        dof = postProcessing.profile.components.Find(x => x.name.StartsWith("DepthOfField")) as DepthOfField;

        pipeline = (UniversalRenderPipelineAsset)QualitySettings.renderPipeline;

        cameraData = camera.GetUniversalAdditionalCameraData();

        loadSettings();
        applyAA(aaIndex);

        aoT.text = $"Ambient Occlusion: {aoTypes[aoIndex]}";
        ao.SetActive(aoIndex > 0);

        dofT.text = $"Depth of Field: {dofTypes[dofIndex]}";
        dof.mode.Override((DepthOfFieldMode)dofIndex);
    }

    public void NextDisplay()
    {
        if (displayIndex == 3) displayIndex = -1;
        displayIndex++;
        Screen.fullScreenMode = (FullScreenMode)displayIndex;
        displayT.text = $"Display: {displayModes[displayIndex]}";
    }

    public void NextResolution()
    {
        if (resolutionIndex == resolutions.Length - 1) resolutionIndex = -1;
        resolutionIndex++;

        Resolution res = resolutions[resolutionIndex];
        Screen.SetResolution(res.width, res.height, Screen.fullScreenMode, res.refreshRate);
        resolutionT.text = $"Resolution: {res.width}x{res.height} @ {res.refreshRate}Hz";
    }

    public void NextQuality()
    {
        if (qualityIndex == quality.Length - 1) qualityIndex = -1;
        qualityIndex++;

        qualityT.text = $"Quality: {quality[qualityIndex]}";

        QualitySettings.SetQualityLevel(qualityIndex);

        pipeline = (UniversalRenderPipelineAsset)QualitySettings.renderPipeline;

        defaultSettings(false);
        applyAA(aaIndex);

        aoT.text = $"Ambient Occlusion: {aoTypes[aoIndex]}";
        ao.SetActive(aoIndex > 0);

        dofT.text = $"Depth of Field: {dofTypes[dofIndex]}";
        dof.mode.Override((DepthOfFieldMode)dofIndex);
    }

    public void NextAO()
    {
        if (aoIndex == aoTypes.Length - 1) aoIndex = -1;
        aoIndex++;

        aoT.text = $"Ambient Occlusion: {aoTypes[aoIndex]}";
        ao.SetActive(aoIndex > 0);
    }

    public void NextAA()
    {
        if (aaIndex == aaTypes.Length - 1) aaIndex = -1;

        applyAA(++aaIndex);
    }

    public void NextDOF()
    {
        if (dofIndex == dofTypes.Length - 1) dofIndex = -1;
        dofIndex++;

        dofT.text = $"Depth of Field: {dofTypes[dofIndex]}";

        dof.mode.Override((DepthOfFieldMode)dofIndex);
    }

    public void Update()
    {
        if(Input.GetKeyUp(KeyCode.Escape))
        {
            Leave();
        }
    }

    public void Leave()
    {
        saveSettings();
        parent.Play("OptionsMenuHide");
    }

    private void applyAA(int setting)
    {
        aaIndex = setting;
        aaT.text = $"Anti-Aliasing: {aaTypes[aaIndex]}";

        int scale = 1;
        bool msaa = false;
        int msaaLevel = 0;
        AntialiasingMode mode = AntialiasingMode.None;
        switch (setting)
        {
            case 1:
                mode = AntialiasingMode.FastApproximateAntialiasing;
                break;
            case 2:
                mode = AntialiasingMode.SubpixelMorphologicalAntiAliasing;
                break;
            case 3:
            case 4:
                msaa = true;
                msaaLevel = (setting - 2) * 2;
                break;
            case 5:
            case 6:
                scale = (setting - 4) * 2;
                break;
        }

        camera.allowMSAA = msaa;
        QualitySettings.antiAliasing = msaaLevel;
        cameraData.antialiasing = mode;
        pipeline.renderScale = scale;
    }
}
