using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ConstructButton : MonoBehaviour, IPointerClickHandler
{
    private GameBoardBehavior board;
    public BuildingType building;

    public void OnPointerClick(PointerEventData eventData)
    {
        board.Construct((byte)board.selected, building);
    }

    // Start is called before the first frame update
    void Start()
    {
        board = FindObjectOfType<GameBoardBehavior>();
    }

    // Update is called once per frame
    void Update()
    {

    }
}