using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MovingChangeButton : MonoBehaviour, IPointerClickHandler
{
    public enum Direction
    {
        Down,
        Up
    }

    public Direction direction;

    public MovingLabel label;

    public void OnPointerClick(PointerEventData eventData)
    {
        label.Change(direction == Direction.Up);
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
