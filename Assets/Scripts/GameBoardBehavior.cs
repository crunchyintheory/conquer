﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent, RequireComponent(typeof(AudioSource))]
public class GameBoardBehavior : MonoBehaviour
{
    [Header("Objects")]
    public GameObject gamePiece;
    public GameObject marker;
    public GameObject hoverMarker;
    public GameObject barracks;
    public GameObject factory;
    
    [Header("UI")]
    public MovingLabel movingLabel;
    public GameObject hexUi;
    public GameObject hexHoverUi;
    public GameObject hexUiConstruct;
    public UnityEngine.UI.Text hexHoverLabel;
    
    [Header("Hex Position")]
    private float hexScale;
    private Quaternion rotation;
    public Vector3 startTopLeft = new Vector3(-0.2355f,0.8808f,0.1500f);

    public float hexWidthBase = 0.01728140625f;
    public float hexHeightBase = 0.02000034375f;
    private float HexWidth { get { return hexWidthBase * hexScale; } }
    private float HexHeight { get { return hexHeightBase * hexScale; } }

    [Header("Board Size")]
    public byte rows = 6;
    public byte cols = 11;

    [Header("Audio")]
    public AudioClip selectClip;
    public AudioClip constructClip;
    public AudioClip moveClip;

    private readonly List<List<HexBehavior>> hexes = new List<List<HexBehavior>>();

    public short selected { get; private set; } = -1;


    private List<GameObject> markerClones = new List<GameObject>();
    private List<short> validMoves;

    private AudioSource audioSource;

    private void deselect()
    {
        selected = -1;
        hexUi.SetActive(false);
        foreach (GameObject obj in markerClones)
        {
            Destroy(obj);
        }
    }

    private Vector2 hexToScreenPosition(HexBehavior hex)
    {
        return Director.Instance.cam.WorldToScreenPoint(hex.transform.position);
    }

    private void showHexHighlightUI(HexBehavior hex)
    {
        RectTransform parent = hexUi.transform.parent as RectTransform;
        RectTransform hexT = hexUi.transform as RectTransform;

        hexT.position = hexToScreenPosition(hex);
        hexT.position += Vector3.down * 70;

        float maxX = parent.rect.width - hexT.rect.width;
        if (maxX < hexT.anchoredPosition.x)
        {
            Vector3 pos = hexT.anchoredPosition;
            pos.x = maxX;
            hexT.anchoredPosition = pos;
        }

        hexUiConstruct.SetActive(hex.structure == BuildingType.None);
        
        hexUi.SetActive(true);
    }

    private HexBehavior getHex(short position)
    {
        return hexes[position & 0x0f][(position & 0xf0) >> 4];
    }

    public void Select(short position)
    {
        if(selected == position)
        {
            deselect();
            audioSource.PlayOneShot(selectClip);
            return;
        }
        bool first = selected == -1;
        short occChange = 0;
        HexBehavior hex;
        if (!first)
        {
            if(!validMoves.Contains(position))
            {
                //deselect();
                return;
            }
            hex = getHex(selected);
            foreach(GameObject obj in markerClones)
            {
                Destroy(obj);
            }

            if (hex.occupation > 0)
            {
                hex.occupation -= movingLabel.Current;
                hex.startOfTurnOccupation -= movingLabel.Current;
                occChange += movingLabel.Current;
            }
            else if (hex.occupation < 0)
            {
                hex.occupation += movingLabel.Current;
                hex.startOfTurnOccupation += movingLabel.Current;
                occChange -= movingLabel.Current;
            }
            audioSource.PlayOneShot(moveClip);
        }
        hex = getHex(position);
        if (first && ( ((Director.Instance.GameStage & 4) == 4 && hex.occupation > 0) || ((Director.Instance.GameStage & 4) == 0 && hex.occupation < 0) ))
        {
            GetValidMoves(hex);
            selected = position;
            movingLabel.Reset((short)Mathf.Abs(hex.startOfTurnOccupation));
            showHexHighlightUI(hex);
            audioSource.PlayOneShot(selectClip);

        } else
        {
            if((hex.occupation > 0 && occChange < 0) || (hex.occupation < 0 && occChange > 0))
            {
                Director.Instance.BlueUnits -= Mathf.Abs(Mathf.Min(Mathf.Abs(hex.occupation), occChange));
                Director.Instance.RedUnits -= Mathf.Abs(Mathf.Min(Mathf.Abs(hex.occupation), occChange));
                hex.startOfTurnOccupation = 0;
            }

            Director.Instance.scoreboard.UpdateBoard(Director.Team.Blue);
            Director.Instance.scoreboard.UpdateBoard(Director.Team.Red);
            hex.occupation += occChange;
            selected = -1;
            hexUi.SetActive(false);

            if (Director.Instance.BlueUnits > 0 && Director.Instance.RedUnits <= 0)
                Director.Instance.GameOver(Director.Team.Blue);
            else if (Director.Instance.BlueUnits <= 0 && Director.Instance.RedUnits > 0)
                Director.Instance.GameOver(Director.Team.Red);
            else if (Director.Instance.BlueUnits <= 0 && Director.Instance.RedUnits <= 0)
                Director.Instance.GameOver(Director.Team.None);
        }
    }

    public void HoverEnter(short position)
    {
        HexBehavior hex = getHex(position);
        if (hex.occupation < 5 && hex.occupation > -5) return;
        RectTransform hexT = hexHoverUi.transform as RectTransform;

        hexT.position = hexToScreenPosition(hex) + (Vector2.up * 25);

        hexHoverLabel.text = Mathf.Abs(hex.occupation).ToString();

        hexHoverUi.SetActive(true);
    }

    public void HoverExit(short position)
    {
        hexHoverUi.SetActive(false);
    }

    public void EndTurn()
    {
        Director.Instance.TurnComplete();

        foreach (List<HexBehavior> arr in hexes)
        {
            foreach (HexBehavior h in arr)
            {
                h.OnTurnStart((Director.Instance.GameStage & 4) == 4 ? Director.Team.Blue : Director.Team.Red);
            }
        }
    }

    internal void GetValidMoves(HexBehavior hex)
    {
        byte row = (byte)((hex.position & 0xf0) >> 4);
        byte col = (byte)(hex.position & 0x0f);

        markerClones = new List<GameObject>();
        validMoves = new List<short>();

        if (hex.startOfTurnOccupation != 0)
        {
            if(row != 0)
            {
                MarkAsValid(row - 1, col);
            }
            if(row != hexes[col].Count - 1)
            {
                MarkAsValid(row + 1, col);
            }
            if(col != 0)
            {
                if (row != 0 && col % 2 != 1)
                {
                    MarkAsValid(row - 1, col - 1);
                }
                if (row < hexes[col - 1].Count)
                {
                    MarkAsValid(row, col - 1);
                }
                if (row < hexes[col - 1].Count - 1 && col % 2 != 0)
                {
                    MarkAsValid(row + 1, col - 1);
                }
            }
            if(col != hexes.Count - 1)
            {
                if (row != 0 && col % 2 != 1)
                {
                    MarkAsValid(row - 1, col + 1);
                }
                if(row < hexes[col + 1].Count)
                {
                    MarkAsValid(row, col + 1);
                }
                if (row != hexes[col + 1].Count - 1 && col % 2 != 0)
                {
                    MarkAsValid(row + 1, col + 1);
                }
            }
        }
        Vector3 t = hex.transform.position;
        t.y += 0.0251f;

        //markerClones.Add(Instantiate(marker, hexes[col][row].transform.position, marker.transform.rotation, transform));
        markerClones.Add(Instantiate(hoverMarker, t, marker.transform.rotation, transform));
    }

    internal void MarkAsValid(int row, int col)
    {
        validMoves.Add((byte)((row << 4) | col));
        markerClones.Add(Instantiate(marker, hexes[col][row].transform.position, marker.transform.rotation, transform));
    }

    // Start is called before the first frame update
    void Start()
    {
        hexScale = gamePiece.transform.localScale.x;
        rotation = gamePiece.transform.rotation;

        audioSource = GetComponent<AudioSource>();
    }

    public void CreateBoard()
    {
        HexBehavior hex;

        Vector3 currentPos = startTopLeft;
        List<HexBehavior> column;
        for (byte i = 0; i < cols; i++)
        {
            column = new List<HexBehavior>();
            currentPos.z = startTopLeft.z - (HexWidth / 2) * (i % 2);
            for (byte j = 0; j < (rows - (i % 2)); j++)
            {
                hex = Instantiate(gamePiece, currentPos, rotation, transform).GetComponent<HexBehavior>();
                currentPos.z -= HexWidth;
                hex.SetPosition(j, i);
                hex.gameObject.name = $"Hex {j}/{i}";
                column.Add(hex);
            }
            currentPos.x += HexHeight * 0.75f;
            hexes.Add(column);
        }

        hexes[0][0].occupation = -1;
        Construct(0, BuildingType.Factory, true);
        hexes[hexes.Count - 1][rows - ((hexes.Count - 1) % 2) - 1].occupation = 1;
        Construct(hexes[hexes.Count - 1][rows - ((hexes.Count - 1) % 2) - 1], BuildingType.Factory, true);

        foreach (List<HexBehavior> arr in hexes)
        {
            foreach (HexBehavior h in arr)
            {
                h.OnTurnStart(Director.Team.Blue);
            }
        }

        hexUi.SetActive(false);

        Director.Instance.AddCoin(Director.Team.Blue, 4);
        Director.Instance.AddCoin(Director.Team.Red, 4);

    }

    public void DestroyBoard()
    {
        foreach (Transform child in transform) Destroy(child.gameObject);
        hexes.Clear();
        selected = -1;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Construct(HexBehavior hex, BuildingType building, bool free = false)
    {
        Vector3 offset;
        GameObject go, template;
        int cost;
        if (building == BuildingType.None)
        {
            Destroy(hex.transform.Find("Structure"));
            return;
        }
        else if (building == BuildingType.Barracks)
        {
            template = barracks;
            offset = new Vector3(0.002093893f, 0.03249999f, 0);
            cost = -5;
        }
        else
        {
            template = factory;
            offset = new Vector3(0.002706391f, 0.03249999f, 0);
            cost = -3;
        }
        if(!free)
        {
            if ((hex.occupation > 0 ? Director.Instance.BlueResources : Director.Instance.RedResources) < -cost)
            {
                return;
            }
            switch (building)
            {
                case BuildingType.Barracks:
                    if (hex.occupation > 0) Director.Instance.stats.BlueBarracks++; else Director.Instance.stats.RedBarracks++;
                    break;
                case BuildingType.Factory:
                    if (hex.occupation > 0) Director.Instance.stats.BlueFactories++; else Director.Instance.stats.RedFactories++;
                    break;
            }
            audioSource.PlayOneShot(constructClip);
            Director.Instance.AddCoin(hex.occupation > 0 ? Director.Team.Blue : Director.Team.Red, cost);
        }
        hex.structure = building;
        go = Instantiate(template, hex.transform.position, template.transform.rotation, hex.transform);
        go.transform.localPosition += offset;
        go.GetComponent<ConstructBehavior>().SetTeam(hex.occupation == 0 ? Director.Team.None : (hex.occupation > 0 ? Director.Team.Blue : Director.Team.Red));
    }
    public void Construct(byte position, BuildingType building, bool free = false)
    {
        HexBehavior hex = hexes[position & 0x0f][(position & 0xf0) >> 4];
        Construct(hex, building, free);
    }
}
