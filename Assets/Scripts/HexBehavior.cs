﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[DisallowMultipleComponent]
public class HexBehavior : MonoBehaviour
{
    public Material neutralMaterial;
    public Material blueMaterial;
    public Material redMaterial;
    public GameObject token;

    public GameObject tokenHolder;

    public short _occupation = 0;
    public short occupation {
        get {
            return _occupation;
        }
        set
        {
            #pragma warning disable UNT0008 // Null propagation on Unity objects
            if (_occupation != value) UpdateOccupation((uint)Mathf.Abs(value));
            if (value < 0)
            {
                // Now under red control
                mesh.material = redMaterial;
                foreach(ConstructBehavior behavior in GetComponentsInChildren<ConstructBehavior>()) behavior.SetTeam(Director.Team.Red);
            }
            else if (value > 0)
            {
                // Now under blue control
                mesh.material = blueMaterial;
                foreach (ConstructBehavior behavior in GetComponentsInChildren<ConstructBehavior>()) behavior.SetTeam(Director.Team.Blue);
            }
            else
            {
                mesh.material = neutralMaterial;
                foreach (ConstructBehavior behavior in GetComponentsInChildren<ConstructBehavior>()) behavior.SetTeam(Director.Team.None);
            }
            _occupation = value;
            #pragma warning restore UNT0008 // Null propagation on Unity objects
        }
    }
    public BuildingType structure = BuildingType.None;
    public byte position = 0;

    public short startOfTurnOccupation;

    private GameBoardBehavior board;
    private MeshRenderer mesh;

    public void Awake()
    {
        board = GetComponentInParent<GameBoardBehavior>();
        mesh = GetComponent<MeshRenderer>();
    }

    public void OnMouseUp()
    {
        board.Select(position);
    }

    public void OnMouseEnter()
    {
        board.HoverEnter(position);
    }

    public void OnMouseExit()
    {
        board.HoverExit(position);
    }

    public void SetPosition(byte row, byte col)
    {
        position = (byte)((row << 4) | col);
    }

    public void OnTurnStart(Director.Team player)
    {
        if(structure == BuildingType.Barracks)
        {
            if(occupation < 0 && player == Director.Team.Red)
            {
                occupation -= 1;
                Director.Instance.RedUnits++;
                Director.Instance.stats.RedUnits++;
            }

            else if (occupation > 0 && player == Director.Team.Blue)
            {
                occupation += 1;
                Director.Instance.BlueUnits++;
                Director.Instance.stats.BlueUnits++;
            }

            Director.Instance.scoreboard.UpdateBoard(player);
        }

        else if (structure == BuildingType.Factory)
        {
            if(occupation < 0 && player == Director.Team.Red)
            {
                Director.Instance.AddCoin(player);
            }
            
            else if (occupation > 0 && player == Director.Team.Blue)
            {
                Director.Instance.AddCoin(player);
            }
            // Add resource token
        }

        startOfTurnOccupation = occupation;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void UpdateOccupation(uint occupation)
    {
        GameObject temp;
        foreach(Transform child in tokenHolder.transform)
        {
            Destroy(child.gameObject);
        }
        
        if (occupation >= 1)
        {
            temp = Instantiate(token, tokenHolder.transform.position, token.transform.rotation, tokenHolder.transform);
            temp.transform.localPosition += new Vector3(-0.005f, 0.03f, 0);
        }
        if (occupation >= 2)
        {
            temp = Instantiate(token, tokenHolder.transform.position, token.transform.rotation, tokenHolder.transform);
            temp.transform.localPosition += new Vector3(-0.005f, 0.03f, 0.002f);
        }
        if (occupation >= 3)
        {
            temp = Instantiate(token, tokenHolder.transform.position, token.transform.rotation, tokenHolder.transform);
            temp.transform.localPosition += new Vector3(-0.005f, 0.03f, -0.002f);
        }
        if (occupation >= 4)
        {
            temp = Instantiate(token, tokenHolder.transform.position, token.transform.rotation, tokenHolder.transform);
            temp.transform.localPosition += new Vector3(-0.007f, 0.03f, 0.001f);
        }
        if (occupation >= 5)
        {
            temp = Instantiate(token, tokenHolder.transform.position, token.transform.rotation, tokenHolder.transform);
            temp.transform.localPosition += new Vector3(-0.007f, 0.03f, -0.001f);
        }
    }
}
